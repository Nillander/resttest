package maxnivel.development.nillander.Comum.Bean;

import java.util.ArrayList;
import org.json.JSONObject;

public class EndPointArquivo {

    public String nome, absolutePath;

    private JSONObject entidadeDocumentacao, metodos, modulo, resposta;

    private ArrayList<JSONOb> arquivos = new ArrayList<>();

    public EndPointArquivo() {
    }

    public EndPointArquivo(String nome) {
        this.nome = nome;
    }

    public EndPointArquivo(String nome, JSONObject entidadeDocumentacao, JSONObject metodos, JSONObject modulo, JSONObject resposta) {
        this.nome = nome;
        this.entidadeDocumentacao = entidadeDocumentacao;
        this.metodos = metodos;
        this.modulo = modulo;
        this.resposta = resposta;
    }

    public EndPointArquivo(String nome, ArrayList<JSONOb> arquivos) {
        this.nome = nome;
        this.arquivos = arquivos;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<JSONOb> getArquivos() {
        return arquivos;
    }

    /*public JSONOb getLocalizarArquivo(EJson arquivo) {
        for (JSONOb j : getArquivos()) {
            if (j.getName() == arquivo) {
                return j;
            }
        }
        return null;
    }*/
    public void setArquivos(ArrayList<JSONOb> arquivos) {
        this.arquivos = arquivos;
    }

}
