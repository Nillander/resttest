package maxnivel.development.nillander.Comum.Bean;

import maxnivel.development.nillander.Comum.Enum.EJson;
import org.json.JSONObject;

public class JSONOb {

    private EJson name;
    private JSONObject object;

    public JSONOb() {
    }

    public JSONOb(EJson name, JSONObject object) {
        this.name = name;
        this.object = object;
    }

    public JSONObject getObject() {
        return object;
    }

    public void setObject(JSONObject object) {
        this.object = object;
    }

    public EJson getName() {
        return name;
    }

    public void setName(EJson name) {
        this.name = name;
    }

}
