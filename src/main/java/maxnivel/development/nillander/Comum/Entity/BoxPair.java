package maxnivel.development.nillander.Comum.Entity;

public class BoxPair<K, V> implements Pair<K, V> {

    private K key;
    private V value;

    /**
     * *
     * construtor vazio;
     */
    public BoxPair() {
    }

    /**
     *
     * @param key chave sem valor
     */
    public BoxPair(K key) {
        this.key = key;
    }

    /**
     *
     * @param key
     * @param value
     */
    public BoxPair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public K getKey() {
        return key;
    }

    @Override
    public String getKeyString() {
        return key.toString();
    }

    @Override
    public V getValue() {
        return value;
    }

    @Override
    public String getValueString() {
        return value.toString();
    }

    @Override
    public BoxPair<K, V> setKey(K key) {
        this.key = key;
        return this;
    }

    @Override
    public BoxPair<K, V> setValue(V value) {
        this.value = value;
        return this;
    }

    @Override
    public BoxPair<K, V> setValue(BoxPair boxpair) {
        this.value = (V) boxpair.getValue();
        return this;
    }

    @Override
    public String toString() {
        return "[" + this.getKeyString() + "] = \"" + this.getValueString() + "\"";
    }

}
