package maxnivel.development.nillander.Comum.Entity;

public class Box<T> {

    private String chave;

    private T valor;

    public Box() {
    }

    public Box(String chave, T valor) {
        this.chave = chave;
        this.valor = valor;
    }

    public String getChave() {
        return chave;
    }

    public Box(String chave) {
        this.chave = chave;
    }

    public void setChave(String chave) {
        this.chave = chave;
    }

    public T getValor() {
        return valor;
    }

    public void setValor(T valor) {
        this.valor = valor;
    }

}
