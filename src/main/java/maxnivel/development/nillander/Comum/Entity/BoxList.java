package maxnivel.development.nillander.Comum.Entity;

import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.function.Predicate;
import java.util.stream.Collectors;

// Preparar métodos para Bread
public class BoxList<E> extends ArrayList<BoxPair> {

    /**
     * objetoBoxPair.add("Chave", value);
     *
     * @param <E>
     * @param key
     * @param e
     * @return
     */
    public <E> boolean add(String key, E e) {
        return super.add(new BoxPair(key, e));
    }

    /**
     * BoxPair<>().ad("CLIENT_ID", null)
     *
     * @param <V>
     * @param key chave
     * @param v valor
     * @return
     */
    public <V> BoxList addIns(String key, V v) {
        return this.addIns(new BoxPair(key, v));
    }

    /**
     * BoxPair<>().ad("chave");
     *
     * @param key
     * @return
     */
    public BoxList addIns(String key) {
        super.add(new BoxPair(key));
        return this;
    }

    /**
     * BoxPair<>().ad(new BoxPair( ---- ));
     *
     * @param bp
     * @return
     */
    public BoxList addIns(BoxPair bp) {
        super.add(bp);
        return this;
    }

    /**
     * Usado para casos onde não há chave. A chave se torna autoincremental
     * fixa.
     *
     * @param <V>
     * @param v
     * @return
     */
    public <V> BoxList insAutoIncrement(V v) {
        return this.addIns(new BoxPair(this.size(), v));
    }

    /**
     * Ao receber um objeto do tipo BoxPair, procura na lista e atualizar a
     * propriedade "Value".
     *
     * @param e
     * @return
     */
    public BoxList<E> set(BoxPair e) {
        this.set(e.getKeyString(), e.getValue());
        return (BoxList<E>) this;
    }

    /**
     * Através da Chave, define um novo valor para o Value, somente caso o Value
     * seja nulo.
     *
     * @param e
     * @return
     */
    public BoxList<E> setCaseNull(BoxPair e) {
        if (getValueBy(e) == null) {
            this.set(e);
        }
        return this;
    }

    /**
     * Recebe a Key e a Value separadas, dessa meneira procura pela Key
     * compatível e atualiza a value.
     *
     * @param <V>
     * @param key
     * @param v
     * @return
     */
    public <V> BoxList<E> set(String key, V v) {
        BoxPair bp = this.get(key);
        if (bp != null) {
            int indexBp = this.indexOf(bp);
            bp.setValue(v);
            this.set(indexBp, bp);
        }
        return this;
    }

    /**
     * Verifica se a chave pesquisada existe dentro da lista.
     *
     * @param key
     * @return
     */
    public boolean check(String key) {
        return !(this.get(key) == null);
    }

    /**
     * Encontra por Chave e returna um objeto do tipo BoxPair de dentro da
     * BoxList, ou retorna nulo caso não exista.
     *
     * @param key
     * @return BoxPair
     */
    public BoxPair get(String key) {
        return this.stream().filter(b -> b.getKeyString().equals(key)).findAny().orElse(null);
    }

    /**
     * Procura na lista um objeto, passando um próprio objeto. No final das
     * contas só pega do objeto passado a chave para pesquisar.
     *
     * @param getBoxPairByKeyStringInBoxPair
     * @return BoxPair
     */
    public BoxPair get(BoxPair getBoxPairByKeyStringInBoxPair) {
        return this.get(getBoxPairByKeyStringInBoxPair.getKeyString());
    }

    /**
     * Procura dentro da lista um objeto com a mesma chave e valor.
     *
     * @param e
     * @return BoxPair
     */
    public BoxPair findMatch(BoxPair e) {
        return this.stream().filter(b -> b.getKeyString().equals(e.getKeyString()) && b.getValue() == e.getValue()).findFirst().orElse(null);
    }

    /**
     * Retorna o valor do objeto, enviando um objeto boxpair, será extraída a
     * sua chave para pesquisar
     *
     * @param <E>
     * @param boxPair
     * @return
     */
    public <E> E getValueBy(BoxPair boxPair) {
        BoxPair retorno = this.get(boxPair.getKeyString());
        return (retorno == null) ? null : (E) retorno.getValue();
        //return (E) this.get(boxPair.getKeyString()).getValue();
    }

    /**
     * Retornar o valor do objeto, pesquisando pela chave.
     *
     * @param <E>
     * @param key
     * @return
     */
    public <E> E getValueBy(String key) {
        BoxPair retorno = this.get(key);
        return (retorno == null) ? null : (E) retorno.getValue();
        //return (E) this.get(key).getValue();
    }

    /**
     * Retornar o valor em forma de String, pesquisando pela Chave.
     *
     * @param key
     * @return
     */
    public String getValueString(String key) {
        BoxPair retorno = this.get(key);
        return (retorno == null) ? null : retorno.getValueString();
        //return this.get(key).getValueString();
    }

    /**
     * Filter results by predicate. Example: filter((BoxPair bp) ->
     * bp.getKeyString().length() == 2));
     *
     * @param predicate
     * @return BoxList
     */
    public BoxList<E> filter(Predicate<E> predicate) {
        return this.stream().filter((Predicate<? super BoxPair>) predicate).collect(Collectors.toCollection(BoxList<E>::new));
    }

    @Override
    public String toString() {
        StringBuilder toStringBuilder = new StringBuilder();
        this.forEach((element) -> {
            if (indexOf(element) > 0) {
                toStringBuilder.append("\n");
            }
            toStringBuilder.append("[").append(this.indexOf(element)).append("]").append(new GsonBuilder().create().toJson(element));
        });
        return toStringBuilder.toString();
    }

}
