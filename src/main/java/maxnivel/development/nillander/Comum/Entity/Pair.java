package maxnivel.development.nillander.Comum.Entity;

public interface Pair<K, V> {

    public Pair setKey(K key);

    public K getKey();

    public String getKeyString();

    public Pair setValue(V value);

    public Pair setValue(BoxPair pair);
    
    public V getValue();

    public String getValueString();

    @Override
    public String toString();
}
