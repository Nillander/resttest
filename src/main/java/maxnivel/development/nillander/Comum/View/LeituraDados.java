package maxnivel.development.nillander.Comum.View;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LeituraDados {

    private static LeituraDados instance = null;

    public static LeituraDados getInstance() {
        if (instance == null) {
            instance = new LeituraDados();
        }
        return instance;
    }

    public String lerDadosTeclado(String mensagem) {
        try {
            System.out.println(mensagem);
            return new BufferedReader(new InputStreamReader(System.in)).readLine();
        } catch (IOException ex) {
            Logger.getLogger(LeituraDados.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
