package maxnivel.development.nillander.Comum.View;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import maxnivel.development.nillander.Comum.Entity.BoxList;
import maxnivel.development.nillander.Comum.Util.Sistema;

public class EscritorDeResultados {

    private static EscritorDeResultados instance = null;

    public static EscritorDeResultados getInstance() {
        instance = (instance == null) ? new EscritorDeResultados() : instance;
        return instance;
    }

    private final BoxList CONFIGURACOES = new BoxList().addIns("ARQUIVO", "resultados/resultado.html");

    private final StringBuilder conteudo = new StringBuilder();

    public void apagarDados() {
        File f = new File(CONFIGURACOES.getValueString("ARQUIVO"));
        // Sistema.print(f.getAbsolutePath());
        // Sistema.print(f.delete());
        f.delete();
    }

    public void escreverArquivo() {
        new File("resultados").mkdirs();

        try (FileWriter writer = new FileWriter(CONFIGURACOES.getValueString("ARQUIVO"), true)) {
            writer.write(this.conteudo.toString());
        } catch (IOException e) {
            escreverResultado(Sistema.allPrint(e));
        }
    }

    public void escreverResultado(String conteudo) {
        this.conteudo.append(conteudo.replace(
                System.getProperty("line.separator"),
                System.getProperty("line.separator") + "<br>"));
    }

}
