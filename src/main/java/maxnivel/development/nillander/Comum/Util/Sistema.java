package maxnivel.development.nillander.Comum.Util;

import com.google.gson.GsonBuilder;
import java.util.ArrayList;

public class Sistema {

    /**
     *
     * @param <E>
     * @param e
     * @return
     */
    private static <E> StringBuilder printHeader(E e) {
        StringBuilder header = new StringBuilder("\n========================================================================");

        StackTraceElement[] currentThread = Thread.currentThread().getStackTrace();
        for (int i = currentThread.length - 1; i > 0; i--) {
            StackTraceElement st = currentThread[i];
            if (!st.getClassName().contains("java")
                    && !st.getClassName().contains(Sistema.class.getName())
                    && ((st.toString().contains("$") && st.toString().contains("lambda")) || !st.toString().contains("$"))) {
                header.append("\n").append(st);
            }
        }
        header.append("\n");
        if (e == null) {
            header.append("NULL OBJECT");
        } else {
            header.append(e.getClass().getName());
        }
        header.append("\n========================================================================\n");
        return header;
    }

    /**
     *
     * @param <E>
     * @param e
     * @param ln
     * @return
     */
    private static <E> String retorno(E e, boolean ln) {
        if (e instanceof ArrayList) {
            return printEachStatement(e, ln);
        }
        StringBuilder header = printHeader(e);
        header.append(gsonPrint(e, ln));
        return header.toString();
    }

    /**
     * 
     * @param <E>
     * @param e
     * @param ln
     * @return 
     */
    private static <E> String gsonPrint(E e, boolean ln) {
        StringBuilder header = new StringBuilder();
        if (e == null) {
            header.append("\"null\"");
        } else {
            if (e.getClass().getName().equals(String.class.getName())) {
                header.append("\"").append(e).append("\"");
            } else {
                if (ln == true) {
                    header.append(new GsonBuilder().setPrettyPrinting().create().toJson(e));
                } else {
                    header.append(new GsonBuilder().create().toJson(e));
                }
            }
        }
        return header.toString();
    }

    /**
     *
     * @param <E>
     * @param e
     */
    public static <E> void print(E e) {
        System.out.println(retorno(e, false));
    }

    public static <E> String printEachStatement(E e, boolean ln) {
        StringBuilder header = printHeader(e);
        ArrayList lista = (ArrayList) e;

        int indexOfObject = 0;
        for (Object objectFromElement : (ArrayList) e) {
            header.append("[").append(indexOfObject).append("]")
                    .append(gsonPrint(objectFromElement, ln)).append("\n");
            indexOfObject++;
        }
        return header.toString();
    }

    /**
     *
     * @param <E>
     * @param e
     * @return
     */
    public static <E> String printEach(E e) {
        String retorno = printEachStatement(e, false);
        System.out.println(retorno);
        return retorno;
    }

    /**
     *
     * @param <E>
     * @param e
     * @return
     */
    public static <E> String printEachLn(E e) {
        String retorno = printEachStatement(e, true);
        System.out.println(retorno);
        return retorno;
    }

    /**
     *
     * @param <E>
     * @param e
     */
    public static <E> void println(E e) {
        System.out.println(retorno(e, true));
    }

    /**
     *
     * @param <E>
     * @param e
     * @return
     */
    public static <E> String getPrint(E e) {
        return retorno(e, false);
    }

    /**
     *
     * @param <E>
     * @param e
     * @return
     */
    public static <E> String getPrintln(E e) {
        return retorno(e, true);
    }

    /**
     *
     * @param <E>
     * @param e
     * @param ln
     * @return
     */
    private static <E> String allPrintStatement(E e, boolean ln) {
        String conteudo = (ln == false) ? getPrint(e) : getPrintln(e);
        System.out.println(conteudo);
        return conteudo;
    }

    /**
     *
     * @param <E>
     * @param e
     * @return
     */
    public static <E> String allPrint(E e) {
        return allPrintStatement(e, false);
    }

    /**
     *
     * @param <E>
     * @param e
     * @return
     */
    public static <E> String allPrintln(E e) {
        return allPrintStatement(e, true);
    }

}
