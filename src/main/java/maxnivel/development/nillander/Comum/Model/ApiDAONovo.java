package maxnivel.development.nillander.Comum.Model;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import maxnivel.development.nillander.Comum.Entity.BoxList;
import maxnivel.development.nillander.Comum.Entity.BoxPair;
import maxnivel.development.nillander.Comum.Util.Sistema;
import maxnivel.development.nillander.Comum.View.EscritorDeResultados;
import org.apache.wink.json4j.JSONException;
import org.apache.wink.json4j.JSONObject;

public class ApiDAONovo {

    private static ApiDAONovo instance = null;

    public static ApiDAONovo getInstance() {
        if (instance == null) {
            instance = new ApiDAONovo();
        }
        return instance;
    }

    private final BoxList<BoxPair> CONFIGURACOES = new BoxList<>()
            .addIns("SERVICE_VERSION", null)
            .addIns("MAXNIVEL_SERVER", null)
            .addIns("ENDPOINT_API_PATH", null)
            .addIns("TOKEN_ENDPOINT", "/auth/token")
            .addIns("CLIENT_ID", null)
            .addIns("CLIENT_SECRET", null);

    public void iniciarMotor2(BoxList<BoxPair> CONFIGURACOES_ATUALIZADAS) {
        /*
        // Atualizar Só os nulos;
        CONFIGURACOES.filter((BoxPair bp) -> bp.getValue() == null).stream().forEach(CONFIG_STREAM -> {
            CONFIG_STREAM.setValue(CONFIGURACOES_ATUALIZADAS);
        });
         */

        CONFIGURACOES_ATUALIZADAS.stream().forEach(atualizacao -> {
            CONFIGURACOES.set(atualizacao);
        });

        EscritorDeResultados.getInstance().escreverResultado(Sistema.allPrint(CONFIGURACOES));

        try {
            String urlBuilder
                    = CONFIGURACOES.getValueString("MAXNIVEL_SERVER")
                    + CONFIGURACOES.getValueString("SERVICE_VERSION")
                    + CONFIGURACOES.getValueString("ENDPOINT_API_PATH");
            String pathWithQueryParams = urlBuilder;

            InputStream restApiStream = null;
            HttpURLConnection restApiConnection = getRestApiConnection(pathWithQueryParams);

            // 1. Solicite um token de acesso.
            String accessToken = requestAccessToken();

            // 2. Invoque a API UserList usando o token de acesso.
            addAuthenticationHeader(restApiConnection, accessToken);
            if (restApiConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                if (restApiConnection.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    if (restApiConnection.getHeaderField("www-authenticate").contains("invalid_token")) {
                        // Essa resposta pode significar que o token expirou. Tente uma nova conexão com um novo token de acesso.
                        String newAccessToken = requestAccessToken();
                        HttpURLConnection restApiConnectionWithNewAccessToken = getRestApiConnection(pathWithQueryParams);
                        addAuthenticationHeader(restApiConnectionWithNewAccessToken, newAccessToken);

                        // Valida a conexão estabelecida usando o novo token.
                        if (restApiConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                            // Essa resposta pode significar que você fez uma solicitação para a API UserList usando a categoria incorreta de ID do serviço e você deve lidar com isso no seu código.
                            System.err.println("1.Acesso insuficiente à API REST do MaxNivel: " + restApiConnection.getResponseMessage());
                            //System.exit(1);
                        }
                        restApiStream = restApiConnectionWithNewAccessToken.getInputStream();
                    }
                } else if (restApiConnection.getResponseCode() == HttpURLConnection.HTTP_FORBIDDEN) {
                    System.err.println("2.Acesso insuficiente à API REST do MaxNivel: " + restApiConnection.getResponseMessage());
                }
                System.err.println("Erro ao chamar a API REST do MaxNivel: " + restApiConnection.getResponseMessage());
                //System.exit(1);
            } else {
                restApiStream = restApiConnection.getInputStream();
            }

            // 3. Processe o resultado JSON. Este exemplo imprime o nome de cada usuário.
            try {
                JSONObject userListApiResult = new JSONObject(restApiStream);
                EscritorDeResultados.getInstance().escreverResultado(Sistema.allPrint(userListApiResult));
            } finally {
                // Limpe os fluxos que você abriu.
                if (restApiStream != null) {
                    restApiStream.close();
                }
            }
        } catch (IOException | JSONException e) {
            EscritorDeResultados.getInstance().escreverResultado(Sistema.allPrintln(e.getMessage()));
        }
    }

    /**
     * Configure a conexão com uma API REST, incluindo o manuseio do Bearer
     * Cabeçalho da solicitação de autenticação que deve estar presente em todas
     * as chamadas da API.
     *
     * @param apiCall A cadeia de URLs que indica a chamada e os parâmetros da
     * API.
     * @return a conexão aberta
     */
    private HttpURLConnection getRestApiConnection(String apiCall) throws IOException {
        //EscritorDeResultados.getInstance().escreverResultado(Sistema.allPrint(apiCall));
        URL restApiUrl = new URL(apiCall);
        HttpURLConnection restApiURLConnection = (HttpURLConnection) restApiUrl.openConnection();
        return restApiURLConnection;
    }

    /**
     * Adicione o cabeçalho de autenticação do Portador HTTP que deve estar
     * presente em todos os Chamada de API.
     *
     * @param restApiURLConnection A conexão aberta com a API REST.
     */
    private void addAuthenticationHeader(HttpURLConnection restApiURLConnection, String accessToken) {
        restApiURLConnection.setRequestProperty("Authorization", "Bearer " + accessToken);
    }

    /**
     * Solicite um token de acesso usando o ID do cliente e o segredo do cliente
     * obtidos do seu ID de serviço. Como os tokens de acesso têm um prazo de
     * validade, é melhor solicitar um token antes de atender às suas
     * solicitações.
     *
     * @return o token de acesso que você pode usar para acessar o Blueworks
     * protegido Recursos ao vivo.
     */
    private String requestAccessToken() throws IOException, JSONException {
        byte[] postData = getRequestBodyForAccessToken();
        int postDataLength = postData.length;

        URL url = new URL(CONFIGURACOES.getValueString("MAXNIVEL_SERVER") + CONFIGURACOES.getValueString("SERVICE_VERSION") + CONFIGURACOES.getValueString("TOKEN_ENDPOINT"));
        //EscritorDeResultados.getInstance().escreverResultado(Sistema.allPrint(url.toString()));
        HttpURLConnection endPointRequestConnection = (HttpURLConnection) url.openConnection();
        endPointRequestConnection.setRequestMethod("POST");
        endPointRequestConnection.setDoOutput(true);
        endPointRequestConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        endPointRequestConnection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        endPointRequestConnection.setInstanceFollowRedirects(false);
        try (DataOutputStream dos = new DataOutputStream(endPointRequestConnection.getOutputStream())) {
            dos.write(postData);
        }

        if (endPointRequestConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
            if (endPointRequestConnection.getResponseCode() == HttpURLConnection.HTTP_MOVED_PERM) {
                String serverURL = endPointRequestConnection.getHeaderField("Location");
                // TODO Substitua o valor da constante BWL_SERVER por este novo URL.
                System.err.println("Defina o valor da constante MAXNIVEL_SERVER como: " + serverURL);
            }
            System.err.println("Erro ao obter um token de acesso. " + endPointRequestConnection.getResponseMessage());
            System.exit(1);
        }

        // Processa o resultado JSON para recuperar o token de acesso.
        String accessToken;
        try (InputStream tokenRequestStream = endPointRequestConnection.getInputStream()) {
            JSONObject tokenRequestResult = new JSONObject(tokenRequestStream);
            accessToken = (String) tokenRequestResult.get("access_token");
        }
        return accessToken;
    }

    /**
     * Obtenha o corpo da solicitação a ser usado para a solicitação POST ao
     * solicitar uma token de acesso.
     */
    private byte[] getRequestBodyForAccessToken() {
        StringBuilder sb = new StringBuilder("grant_type=client_credentials");
        sb.append("&client_id=")
                .append(CONFIGURACOES.getValueString("CLIENT_ID"))
                .append("&client_secret=")
                .append(CONFIGURACOES.getValueString("CLIENT_SECRET"));
        return sb.toString().getBytes(StandardCharsets.UTF_8);
    }
}
