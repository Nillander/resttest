package maxnivel.development.nillander.Comum.Control;

import java.awt.Frame;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import maxnivel.development.nillander.Comum.Bean.EndPointArquivo;
import maxnivel.development.nillander.Comum.Bean.JSONOb;
import maxnivel.development.nillander.Comum.Enum.EJson;

public class CarregadorDeArquivo {

    private static CarregadorDeArquivo instance = null;

    public static CarregadorDeArquivo getInstance() {
        if (instance == null) {
            instance = new CarregadorDeArquivo();
        }
        return instance;
    }

    static boolean exibirJFRAME = false;

    public static ArrayList<EndPointArquivo> carregarPastas(String absolutePath) {
        ArrayList<EndPointArquivo> pastas = new ArrayList<>();
        getPastasDoLocal(absolutePath).stream().forEach(pastaEndPoint -> {

            EndPointArquivo endpoint = new EndPointArquivo(pastaEndPoint.getName());
            ArrayList<JSONOb> arquivos = new ArrayList<>();
            getArquivosJsonsDoLocal(pastaEndPoint.getAbsolutePath() + "/Compilador/").forEach((f) -> {
                arquivos.add(
                        new JSONOb(
                                EJson.valueOf(f.getName().replace(".json", "")),
                                HandlerJson.getInstance().getJsonDoArquivo(f.getAbsolutePath())
                        )
                );
            });
            endpoint.setArquivos(arquivos);

            exibirJFRAME(endpoint);

            pastas.add(new EndPointArquivo(pastaEndPoint.getName(), arquivos));
        });
        return pastas;
    }

    private static void exibirJFRAME(EndPointArquivo endpoint) {
        if (exibirJFRAME) {
            endpoint.getArquivos().stream().forEach(job -> {
                NewJFrame frame = new NewJFrame();
                frame.setTitle(job.getName().toString());
                frame.getjTextArea1().setText(
                        job.getObject().toString().replace(",", ",\n")
                );
                if (!"Modulo".contains(job.getName().toString())) {
                    frame.setExtendedState(Frame.MAXIMIZED_BOTH);
                    frame.setLocationRelativeTo(null);
                    frame.setVisible(true);
                }
                System.out.println(job.getObject());
            });
        }
    }

    public static ArrayList<File> getPastasDoLocal(String absolutePath) {
        return new ArrayList<>(
                Arrays.asList(
                        new File(absolutePath).listFiles(File::isDirectory)
                )
        );
    }

    public static ArrayList<File> getArquivosJsonsDoLocal(String absolutePath) {
        ArrayList<File> list = new ArrayList<>();
        Arrays.asList(new File(absolutePath).listFiles(File::isFile)).stream().filter((f) -> (f.toString().endsWith(".json") == true)).forEachOrdered((f) -> {
            list.add(f);
        });
        return list;
    }
}
