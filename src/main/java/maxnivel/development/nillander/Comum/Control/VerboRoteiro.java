package maxnivel.development.nillander.Comum.Control;

import maxnivel.development.nillander.Comum.Entity.BoxList;
import maxnivel.development.nillander.Comum.Entity.BoxPair;
import maxnivel.development.nillander.Comum.Entity.VerboAbstrato;
import maxnivel.development.nillander.Comum.Enum.EMetodo;
import maxnivel.development.nillander.Comum.Util.Sistema;

public class VerboRoteiro extends VerboAbstrato {

    private static VerboRoteiro instance = null;

    public static VerboRoteiro getInstance() {
        instance = (instance == null) ? new VerboRoteiro() : instance;
        return instance;
    }

    public VerboRoteiro() {

    }

    public void init(BoxList<BoxPair> conteudo) {
        super.E_metodo = EMetodo.valueOf(conteudo.getValueBy("metodo"));
        super.B_requer_oauth = (conteudo.getValueBy("requer_oauth") == "1");
        super.S_oauth_escopo = conteudo.getValueBy("oauth_escopo");
        super.B_ativo = conteudo.getValueBy("ativo");

        Sistema.print(super.E_metodo);
        Sistema.print(super.B_requer_oauth);
        Sistema.print(super.S_oauth_escopo);
        Sistema.print(super.B_ativo);
        /*super.metodo = EMetodo.valueOf(endPointComMetodos.getKeyString());
        requer_oauth;
        oauth_escopo;
        ativo;
         */
    }
 
    public static void main(String[] args) {
        BoxPair metodo = new BoxPair("requer_oauth", "1");
        System.out.println(true);
    }
    
}
