package maxnivel.development.nillander.Comum.Control;

import java.util.ArrayList;
import maxnivel.development.nillander.Comum.Bean.EndPointArquivo;
import maxnivel.development.nillander.Comum.Bean.JSONOb;
import maxnivel.development.nillander.Comum.Entity.BoxList;
import maxnivel.development.nillander.Comum.Entity.BoxPair;
import maxnivel.development.nillander.Comum.Model.ApiDAONovo;
import maxnivel.development.nillander.Comum.Util.Sistema;
import maxnivel.development.nillander.Comum.View.EscritorDeResultados;
import maxnivel.development.nillander.Comum.View.LeituraDados;
import org.json.JSONArray;
import org.json.JSONObject;

public class RestTest {

    private static RestTest instance = null;

    public static RestTest getInstance(String secret) {
        instance = (instance == null) ? new RestTest(secret) : instance;
        return instance;
    }

    public final BoxList<BoxPair> CONFIGURACOES = new BoxList<>()
            .addIns("DESTINO_API", null);

    private RestTest(String secret) {
        if ("d74ed7362dff14f7e3ae5f149e4ed1bd".equals(secret)) {
            iniciarMotor();
            EscritorDeResultados.getInstance().escreverArquivo();
        } else {
            EscritorDeResultados.getInstance().escreverResultado(Sistema.allPrint("Acesso não autorizado"));
            System.exit(0);
        }

    }

    public final void iniciarMotor() {
        limparResultados();
        CONFIGURACOES.set("DESTINO_API", LeituraDados.getInstance().lerDadosTeclado("Destino da Api:"));
        CONFIGURACOES.addIns("SERVICE_VERSION", "/v1").addIns("TOKEN_ENDPOINT", "/auth/token");
        if (CONFIGURACOES.getValueBy("DESTINO_API").equals("")) {
            CONFIGURACOES.set("DESTINO_API", "/home/maxnivel/NetBeansProjects/RestTest/ApiDataCompilada/Servicos/V1");
            CONFIGURACOES
                    .addIns("TOKEN_ENDPOINT", "/auth/token")
                    .addIns("MAXNIVEL_SERVER", "http://nillanderapi.dev.boxnivel.com/api")
                    .addIns("CLIENT_ID", "Java_74e51893719904")
                    .addIns("CLIENT_SECRET", "a9a2a264232c6997b31dee2913b6aeb61e5ed062");
        } else {
            CONFIGURACOES
                    .addIns("MAXNIVEL_SERVER", LeituraDados.getInstance().lerDadosTeclado("URL: \"http://cliente.com.br/api\": "))
                    .addIns("CLIENT_ID", LeituraDados.getInstance().lerDadosTeclado("CLIENT_ID: "))
                    .addIns("CLIENT_SECRET", LeituraDados.getInstance().lerDadosTeclado("CLIENT_SECRET: "));
        }
        ArrayList<EndPointArquivo> listaEndPoint = CarregadorDeArquivo.carregarPastas(CONFIGURACOES.getValueBy("DESTINO_API"));
        iniciarVerificacaoMultiThread(listaEndPoint);
    }

    private void limparResultados() {
        EscritorDeResultados.getInstance().apagarDados();
    }

    private void iniciarVerificacaoMultiThread(ArrayList<EndPointArquivo> listaEndPoint) {
        //new Thread(() -> {
        listaEndPoint.parallelStream().forEach((EndPointArquivo endPoint) -> {
            //new Thread(() -> {

            endPoint.getArquivos().stream().forEach(j -> {
                switch (j.getName()) {
                    case Entidade-> {
                    }

                    case Metodos-> {
                        testarMetodosMotor(endPoint, j);
                    }

                    case Modulo-> {
                    }

                    case Resposta-> {
                    }

                    default-> {
                    }
                }
            });
            //}).start();
        });
        //}).start();
    }

    private void testarMetodosMotor(EndPointArquivo endPoint, JSONOb metodo) {
        BoxList<BoxPair> noMetodos = new BoxList<>();
        metodo.getObject().keySet().stream().forEach(sMetodo -> {
            noMetodos.add(sMetodo, produzirListaDeParesDosMetodos(metodo.getObject().get(sMetodo)));
        });
        testarMetodos(new BoxPair(endPoint.getNome(), noMetodos));
    }

    private void testarMetodos(BoxPair endPointComMetodos) {
        for (BoxPair metodo : (BoxList<BoxPair>) endPointComMetodos.getValue()) {
            BoxList<BoxPair> conteudo = (BoxList<BoxPair>) metodo.getValue();
            if (conteudo.check("documentacao_padrao")) {
                Sistema.print(conteudo);
                VerboRoteiro.getInstance().init(conteudo);
            }
        }

        System.exit(0);
        for (BoxPair metodo : (BoxList<BoxPair>) endPointComMetodos.getValue()) {
            BoxList<BoxPair> conteudo = (BoxList<BoxPair>) metodo.getValue();
            if (conteudo.check("documentacao_padrao")) {
                Sistema.print(conteudo);
            }
        }

        System.exit(0);
        ApiDAONovo.getInstance().iniciarMotor2(new BoxList<>()
                .addIns("ENDPOINT_API_PATH", "/" + endPointComMetodos.getKeyString().toLowerCase())
        );
    }

    private Object produzirListaDeParesDosMetodos(Object object) {
        if (object instanceof JSONArray) {
            /* Array de Métodos? get[ {'tipo':'publico'} , {'tipo':'privado'} ]; */
            EscritorDeResultados.getInstance().escreverResultado(Sistema.allPrint("(object instanceof JSONArray)::" + object));
            return null;
        } else {
            // EscritorDeResultados.getInstance().escreverResultado(Sistema.allPrint(object));
            BoxList<BoxPair> boxNo = new BoxList<>();
            JSONObject json = new JSONObject(object.toString());

            // ler todo conteudo do json
            json.keySet().forEach((chaveNome) -> {
                if (json.get(chaveNome) instanceof JSONArray) {

                    JSONArray jar = json.getJSONArray(chaveNome);
                    System.out.print(chaveNome + "(array)::" + jar);

                    BoxList<BoxPair> conteudoListaNos = new BoxList<>();
                    for (int i = 0; i < jar.length(); i++) { // Não pode ser list { percorre o array / pedidos.get(1) lista em si }
                        //<editor-fold defaultstate="collapsed" desc="instrução">
                        /* Caso dê problema ao ler o objeto passado nesta linha, deve instanciar o objeto sendo do tipo correto, caso seja ele JSONArray ou JSONObject
                        if (jar.get(i) instanceof JSONArray){
                            método(new JSONArray(jar.get(i).toString()));
                        }
                         */
                        //</editor-fold>
                        conteudoListaNos.insAutoIncrement(produzirListaDeParesDosMetodos(jar.get(i).toString()));
                        conteudoListaNos.insAutoIncrement(produzirListaDeParesDosMetodos(jar.get(i).toString()));
                    }
                    boxNo.add(chaveNome, conteudoListaNos);

                } else {
                    boxNo.add(chaveNome, json.get(chaveNome));
                }
            });
            return boxNo;
        }
    }

    private void testarCaseMetodo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
