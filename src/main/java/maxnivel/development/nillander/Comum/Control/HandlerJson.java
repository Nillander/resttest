package maxnivel.development.nillander.Comum.Control;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import maxnivel.development.nillander.Comum.Util.Sistema;
import maxnivel.development.nillander.Comum.View.EscritorDeResultados;
import org.json.JSONObject;

public class HandlerJson {

    private static HandlerJson instance = null;

    public static HandlerJson getInstance() {
        if (instance == null) {
            instance = new HandlerJson();
        }
        return instance;
    }

    public JSONObject getJsonDoArquivo(String absolutePath) {
        try {
            String entidadeStringJson = new String(Files.readAllBytes(Paths.get(absolutePath)));
            return new JSONObject(entidadeStringJson);
        } catch (IOException e) {
            EscritorDeResultados.getInstance().escreverResultado(Sistema.allPrint(absolutePath + " :: Arquivo não foi encontrado.\n" + e.getMessage()));
        }
        return null;
    }

}
