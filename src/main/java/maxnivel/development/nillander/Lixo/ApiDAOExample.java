package maxnivel.development.nillander.Lixo;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import maxnivel.development.nillander.Comum.Util.Sistema;
import org.apache.wink.json4j.JSONException;
import org.apache.wink.json4j.JSONObject;

public class ApiDAOExample {

    /**
     * URL DE ACESSO DA API.
     */
    private final static String BWL_SERVER = "http://nillander.dev.boxnivel.com/api/v1"; // TODO: substitua isso pelo endereço do servidor do Blueworks Live.

    /**
     * O caminho para o endpoint da API.
     */
    private final static String USERLIST_API_PATH = BWL_SERVER + "/pedidos";

    /**
     * A versão da API que você quer usar.
     */
    // private static final String USERLIST_VERSION = "v1";
    /**
     * Caminho para o endpoint do token de autenticação.
     */
    private final static String TOKEN_ENDPOINT = BWL_SERVER + "/auth/token";

    /**
     * O ID do cliente e o segredo do cliente para o Service ID que acessa as
     * APIs REST.
     */
    private final static String CLIENT_ID = "Java_59753409147f66";
    private final static String CLIENT_SECRET = "978fc21b59a4f4d9cd95accff294f291b6f4c66d";

    public static void main(String[] args) {
        System.out.println("main");
        try {
            String urlBuilder = USERLIST_API_PATH;
            String pathWithQueryParams = urlBuilder;

            InputStream restApiStream = null;
            HttpURLConnection restApiConnection = getRestApiConnection(pathWithQueryParams);

            // 1. Solicite um token de acesso.
            String accessToken = requestAccessToken();

            // 2. Invoque a API UserList usando o token de acesso.
            addAuthenticationHeader(restApiConnection, accessToken);
            if (restApiConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                if (restApiConnection.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    if (restApiConnection.getHeaderField("www-authenticate").contains("invalid_token")) {
                        // Essa resposta pode significar que o token expirou. Tente uma nova conexão com um novo token de acesso.
                        String newAccessToken = requestAccessToken();
                        HttpURLConnection restApiConnectionWithNewAccessToken = getRestApiConnection(pathWithQueryParams);
                        addAuthenticationHeader(restApiConnectionWithNewAccessToken, newAccessToken);

                        // Valida a conexão estabelecida usando o novo token.
                        if (restApiConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                            System.err.println("Error calling the Blueworks Live REST API: " + restApiConnection.getResponseMessage());
                            System.exit(1);
                        }
                        restApiStream = restApiConnectionWithNewAccessToken.getInputStream();
                    }
                } else if (restApiConnection.getResponseCode() == HttpURLConnection.HTTP_FORBIDDEN) {
                    // Essa resposta pode significar que você fez uma solicitação para a API UserList usando a categoria incorreta de ID do serviço e
                    // você deve lidar com isso no seu código.
                    System.err.println("Insufficient access to the Blueworks Live REST API: " + restApiConnection.getResponseMessage());
                }
                System.err.println("Error calling the Blueworks Live REST API: " + restApiConnection.getResponseMessage());
                System.exit(1);
            } else {
                restApiStream = restApiConnection.getInputStream();
            }

            // 3. Processe o resultado JSON. Este exemplo imprime o nome de cada usuário.
            try {
                JSONObject userListApiResult = new JSONObject(restApiStream);
                Sistema.print(userListApiResult);
                // JSONArray users = (JSONArray) userListApiResult.get ("users");
                // para (Usuário do objeto: usuários) {
                // Sistema.print (usuário);
                //System.out.println("User name = "+ ((JSONObject) user) .get (" name "));
                //}
            } finally {
                // Limpe os fluxos que você abriu.
                if (restApiStream != null) {
                    restApiStream.close();
                }
            }
        } catch (IOException | JSONException e) {
            // Manipule as exceções que possam ocorrer.
            // Realiza o tratamento de exceções adequado ao seu aplicativo, o que pode incluir diferenças
            // o tipo de exceção e a manipulação apropriada. Por exemplo, você pode querer lidar com
            // problemas de autenticação separadamente, para que o usuário saiba que suas credenciais causaram o problema.
            System.out.println("printStack");
            e.printStackTrace();
        }
    }

    /**
     * Configure a conexão com uma API REST, incluindo o manuseio do Bearer
     * Cabeçalho da solicitação de autenticação que deve estar presente em todas
     * as chamadas da API.
     *
     * @param apiCall A cadeia de URLs que indica a chamada e os parâmetros da
     * API.
     * @retorne a conexão aberta
     */
    private static HttpURLConnection getRestApiConnection(String apiCall) throws IOException {
        System.out.println(apiCall);
        URL restApiUrl = new URL(apiCall);
        HttpURLConnection restApiURLConnection = (HttpURLConnection) restApiUrl.openConnection();
        return restApiURLConnection;
    }

    /**
     * Adicione o cabeçalho de autenticação do Portador HTTP que deve estar
     * presente em todos os Chamada de API.
     *
     * @param restApiURLConnection A conexão aberta com a API REST.
     */
    private static void addAuthenticationHeader(HttpURLConnection restApiURLConnection, String accessToken) {
        restApiURLConnection.setRequestProperty("Authorization", "Bearer " + accessToken);
    }

    /**
     * Solicite um token de acesso usando o ID do cliente e o segredo do cliente
     * obtidos do seu ID de serviço. Como os tokens de acesso têm um prazo de
     * validade, é melhor solicitar um token antes de atender às suas
     * solicitações.
     *
     * @return o token de acesso que você pode usar para acessar o Blueworks
     * protegido Recursos ao vivo.
     */
    private static String requestAccessToken() throws IOException, JSONException {
        byte[] postData = getRequestBodyForAccessToken();
        int postDataLength = postData.length;

        URL url = new URL(TOKEN_ENDPOINT);
        System.out.println(url.toString());
        HttpURLConnection endPointRequestConnection = (HttpURLConnection) url.openConnection();
        endPointRequestConnection.setRequestMethod("POST");
        endPointRequestConnection.setDoOutput(true);
        endPointRequestConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        endPointRequestConnection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        endPointRequestConnection.setInstanceFollowRedirects(false);
        try (DataOutputStream dos = new DataOutputStream(endPointRequestConnection.getOutputStream())) {
            dos.write(postData);
        }

        if (endPointRequestConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
            if (endPointRequestConnection.getResponseCode() == HttpURLConnection.HTTP_MOVED_PERM) {
                String serverURL = endPointRequestConnection.getHeaderField("Location");
                // TODO Substitua o valor da constante BWL_SERVER por este novo URL.
                System.err.println("Set the value of the BWL_SERVER constant to: " + serverURL);
            }
            System.err.println("Error in obtaining an access token. " + endPointRequestConnection.getResponseMessage());
            System.exit(1);
        }

        // Processa o resultado JSON para recuperar o token de acesso.
        String accessToken;
        try (InputStream tokenRequestStream = endPointRequestConnection.getInputStream()) {
            JSONObject tokenRequestResult = new JSONObject(tokenRequestStream);
            accessToken = (String) tokenRequestResult.get("access_token");
        }
        return accessToken;
    }

    /**
     * Obtenha o corpo da solicitação a ser usado para a solicitação POST ao
     * solicitar uma token de acesso.
     */
    private static byte[] getRequestBodyForAccessToken() {
        StringBuilder sb = new StringBuilder("grant_type=client_credentials");
        sb.append("&client_id=")
                .append(CLIENT_ID)
                .append("&client_secret=")
                .append(CLIENT_SECRET);
        return sb.toString().getBytes(StandardCharsets.UTF_8);
    }
}
