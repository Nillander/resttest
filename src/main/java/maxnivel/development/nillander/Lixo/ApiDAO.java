package maxnivel.development.nillander.Lixo;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import maxnivel.development.nillander.Comum.Entity.BoxList;
import maxnivel.development.nillander.Comum.Entity.BoxPair;
import maxnivel.development.nillander.Comum.Util.Sistema;
import org.apache.wink.json4j.JSONException;
import org.apache.wink.json4j.JSONObject;

public class ApiDAO {

    private BoxList<BoxPair> apiSettings;
    private String SERVICE_VERSION = "v1/";
    private String MAXNIVEL_SERVER = "http://nillander.dev.boxnivel.com/api/" + SERVICE_VERSION;
    private String SERVICE_API_PATH = MAXNIVEL_SERVER + "pedidos";
    private String TOKEN_ENDPOINT = MAXNIVEL_SERVER + "auth/token";
    private String CLIENT_ID = "Java_59753409147f66";
    private String CLIENT_SECRET = "978fc21b59a4f4d9cd95accff294f291b6f4c66d";

    private static ApiDAO instance = null;

    private static ApiDAO getInstance() {
        if (instance == null) {
            instance = new ApiDAO();
        }
        return instance;
    }

    public static void main(String[] args) {

    }

    public JSONObject motor(
            String SERVICE_VERSION, String MAXNIVEL_SERVER,
            String SERVICE_API_PATH, String TOKEN_ENDPOINT,
            String CLIENT_ID, String CLIENT_SECRET, BoxList<BoxPair> settings) {
        settings.forEach((bp) -> {
            apiSettings.set(bp);
        });

        apiSettings.stream().filter((bp) -> (bp.getValue() == null)).forEachOrdered((bp) -> {
            apiSettings.set(bp.setValue(bp));
        });

        if (SERVICE_VERSION != null) {
            this.SERVICE_VERSION = SERVICE_VERSION;
        }

        if (MAXNIVEL_SERVER != null) {
            this.MAXNIVEL_SERVER = MAXNIVEL_SERVER;
        }

        if (SERVICE_API_PATH != null) {
            this.SERVICE_API_PATH = SERVICE_API_PATH;
        }

        if (TOKEN_ENDPOINT != null) {
            this.TOKEN_ENDPOINT = TOKEN_ENDPOINT;
        }

        if (CLIENT_ID != null) {
            this.CLIENT_ID = CLIENT_ID;
        }

        if (CLIENT_SECRET != null) {
            this.CLIENT_SECRET = CLIENT_SECRET;
        }

        try {
            String urlBuilder = SERVICE_API_PATH;
            String pathWithQueryParams = urlBuilder;

            InputStream restApiStream = null;
            HttpURLConnection restApiConnection = getRestApiConnection(pathWithQueryParams);

            // 1. Solicite um token de acesso.
            String accessToken = requestAccessToken();

            // 2. Invoque a API UserList usando o token de acesso.
            addAuthenticationHeader(restApiConnection, accessToken);
            if (restApiConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                if (restApiConnection.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    if (restApiConnection.getHeaderField("www-authenticate").contains("invalid_token")) {
                        // Essa resposta pode significar que o token expirou. Tente uma nova conexão com um novo token de acesso.
                        String newAccessToken = requestAccessToken();
                        HttpURLConnection restApiConnectionWithNewAccessToken = getRestApiConnection(pathWithQueryParams);
                        addAuthenticationHeader(restApiConnectionWithNewAccessToken, newAccessToken);

                        // Valida a conexão estabelecida usando o novo token.
                        if (restApiConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                            // Essa resposta pode significar que você fez uma solicitação para a API UserList usando a categoria incorreta de ID do serviço e você deve lidar com isso no seu código.
                            System.err.println("Insufficient access to the Blueworks Live REST API: " + restApiConnection.getResponseMessage());
                            System.exit(1);
                        }
                        restApiStream = restApiConnectionWithNewAccessToken.getInputStream();
                    }
                } else if (restApiConnection.getResponseCode() == HttpURLConnection.HTTP_FORBIDDEN) {
                    System.err.println("Insufficient access to the Blueworks Live REST API: " + restApiConnection.getResponseMessage());
                }
                System.err.println("Error calling the Blueworks Live REST API: " + restApiConnection.getResponseMessage());
                System.exit(1);
            } else {
                restApiStream = restApiConnection.getInputStream();
            }

            // 3. Processe o resultado JSON. Este exemplo imprime o nome de cada usuário.
            try {
                JSONObject userListApiResult = new JSONObject(restApiStream);
                Sistema.print(userListApiResult);
                return userListApiResult;
            } finally {
                // Limpe os fluxos que você abriu.
                if (restApiStream != null) {
                    restApiStream.close();
                }
            }
        } catch (IOException | JSONException e) {
            System.out.println("printStack");
            e.printStackTrace();
        }
        return null;
    }

    private HttpURLConnection getRestApiConnection(String apiCall) throws IOException {
        System.out.println(apiCall);
        URL restApiUrl = new URL(apiCall);
        HttpURLConnection restApiURLConnection = (HttpURLConnection) restApiUrl.openConnection();
        return restApiURLConnection;
    }

    private void addAuthenticationHeader(HttpURLConnection restApiURLConnection, String accessToken) {
        restApiURLConnection.setRequestProperty("Authorization", "Bearer " + accessToken);
    }

    private String requestAccessToken() throws IOException, JSONException {
        byte[] postData = getRequestBodyForAccessToken();
        int postDataLength = postData.length;

        URL url = new URL(TOKEN_ENDPOINT);
        System.out.println(url.toString());
        HttpURLConnection endPointRequestConnection = (HttpURLConnection) url.openConnection();
        endPointRequestConnection.setRequestMethod("POST");
        endPointRequestConnection.setDoOutput(true);
        endPointRequestConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        endPointRequestConnection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        endPointRequestConnection.setInstanceFollowRedirects(false);
        try (DataOutputStream dos = new DataOutputStream(endPointRequestConnection.getOutputStream())) {
            dos.write(postData);
        }

        if (endPointRequestConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
            if (endPointRequestConnection.getResponseCode() == HttpURLConnection.HTTP_MOVED_PERM) {
                String serverURL = endPointRequestConnection.getHeaderField("Location");
                // TODO Substitua o valor da constante BWL_SERVER por este novo URL.
                System.err.println("Set the value of the BWL_SERVER constant to: " + serverURL);
            }
            System.err.println("Error in obtaining an access token. " + endPointRequestConnection.getResponseMessage());
            System.exit(1);
        }

        // Processa o resultado JSON para recuperar o token de acesso.
        String accessToken;
        try (InputStream tokenRequestStream = endPointRequestConnection.getInputStream()) {
            JSONObject tokenRequestResult = new JSONObject(tokenRequestStream);
            accessToken = (String) tokenRequestResult.get("access_token");
        }
        return accessToken;
    }

    private byte[] getRequestBodyForAccessToken() {
        StringBuilder sb = new StringBuilder("grant_type=client_credentials");
        sb.append("&client_id=")
                .append(CLIENT_ID)
                .append("&client_secret=")
                .append(CLIENT_SECRET);
        return sb.toString().getBytes(StandardCharsets.UTF_8);
    }

}
