package maxnivel.development.nillander.Lixo;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.logging.Level;
import maxnivel.development.nillander.Comum.Util.Sistema;
import org.json.JSONException;
import org.json.JSONObject;

public class WebServiceDAO {

    private static final String USER_AGENT = "Mozilla/5.0";

    public static void main(String[] args) {
        try {
            String resposta;
            //resposta = sendGet("https://swapi.co/api/people/?format=json");
            resposta = sendGet("http://maxnivel.local/api/v1/pedidos?access_token=8bb0cae1a5a11f7f2076d62a39b0723155eeab77");
            JSONObject job = new JSONObject(resposta);
            Sistema.println(job);

        } catch (Exception exception) {
            exception.printStackTrace();
        }

        System.exit(0);
        JSONObject jsonObject = null;
        try {
            jsonObject = readJsonFromUrl("https://swapi.co/api/people/?format=json");
            System.out.println(jsonObject);
            for (String sMetodo : jsonObject.keySet()) {
                Sistema.println(jsonObject);
            }
            Sistema.println(jsonObject);

            //System.out.println(jsonObject.toString().replace("\",", "\",\n"));
        } catch (IOException | JSONException ex) {
            java.util.logging.Logger.getLogger(WebServiceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.exit(0);

        if (jsonObject != null && jsonObject.length() > 0) {
            for (int i = 0; i < jsonObject.length(); i++) {
                JSONObject jsonO;
                try {
//                    jsonO = new JSONObject(jsonObject.get(i).toString());
                    //lista.add(resultJsonToUsuario(jsonO));
                } catch (JSONException ex) {
                    java.util.logging.Logger.getLogger(WebServiceDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        System.out.println("WebServiceDAO/readJsonFromUrl: " + url);

        try (InputStream is = new URL(url).openStream()) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText;
            /* readAll init*/
            StringBuilder sb = new StringBuilder();
            int cp;

            while ((cp = rd.read()) != -1) {
                sb.append((char) cp);
            }
            jsonText = sb.toString();
            /* readAll ends*/

            JSONObject json = new JSONObject(jsonText);
            return json;
        }
    }

    
    
    // HTTP GET request
    private static String sendGet(String url) throws Exception {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        StringBuilder response;
        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
        }

        return response.toString();

    }

    // HTTP POST request
    private static void sendPost(String url, String urlParameters, String method) throws Exception {

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        //add reuqest header
        con.setRequestMethod(method);
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        //String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";
        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());

    }

}
