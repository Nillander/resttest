package maxnivel.development.nillander.Metodo.Bean;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import maxnivel.development.nillander.Comum.Entity.BoxPair;
import maxnivel.development.nillander.Comum.Entity.BoxList;
import maxnivel.development.nillander.Comum.Util.Sistema;
import org.json.JSONObject;

public class MetodosJson {

    public static void main(String[] args) {
        /* Pedidos */
        BoxList<BoxPair> listaFilhosPedidos = new BoxList<>();

        BoxList<BoxPair> p1 = new BoxList<>();
        //<editor-fold defaultstate="collapsed" desc="p1">
        p1.add("id", 24562);
        p1.add("status", true);
        p1.add("loja_nome", "LojaVirtualPadrão");
        p1.add("valor_unitario", 100.50);
        p1.add("data_adicionado", "2017-12-07 12:05:05");
        //</editor-fold>

        BoxList<BoxPair> p2 = new BoxList<>();
        //<editor-fold defaultstate="collapsed" desc="p2">
        p2.add(new BoxPair("id", 157));
        p2.add("status", false);
        p2.add("loja_nome", "Loja Qualquer");
        p2.add("valor_unitario", 9.99);
        p2.add("data_adicionado", "1993-06-11 12:05:05");
        //</editor-fold>

        Sistema.print(p2.get("loja_nome"));
        Sistema.print(p2.get("loja_nome").toString());
        
        Sistema.print(p2.filter((BoxPair bp) -> bp.getKeyString().length() == 2));
        Sistema.println(p2.filter((BoxPair bp) -> bp.getKeyString().length() == 2));

        Sistema.println("GENÉRICO");
        
        generico();
    }

    private static void generico() {

        List<BoxPair> listaFilhosTeste = new ArrayList<>();

        /*Adicionar NOS FILHOS na lista*/
        listaFilhosTeste.add(new BoxPair("nome", "teste"));
        listaFilhosTeste.add(new BoxPair("ativo", true));

        /* Pedidos */
        List<BoxPair> listaFilhosPedidos = new ArrayList<>();

        List<BoxPair> p1 = new ArrayList<>();
        p1.add(new BoxPair("id", 24562));
        p1.add(new BoxPair("status", true));
        p1.add(new BoxPair("loja_nome", "LojaVirtualPadrão"));
        p1.add(new BoxPair("valor_unitario", 100.50));
        p1.add(new BoxPair("data_adicionado", "2017-12-07 12:05:05"));

        List<BoxPair> p2 = new ArrayList<>();
        p2.add(new BoxPair("id", 157));
        p2.add(new BoxPair("status", false));
        p2.add(new BoxPair("loja_nome", "Loja Qualquer"));
        p2.add(new BoxPair("valor_unitario", 9.99));
        p2.add(new BoxPair("data_adicionado", "1993-06-11 12:05:05"));

        listaFilhosPedidos.add(new BoxPair(listaFilhosPedidos.size(), p1));
        listaFilhosPedidos.add(new BoxPair(listaFilhosPedidos.size(), p2));

        listaFilhosTeste.add(new BoxPair("pedidos", listaFilhosPedidos));

        /*Adicionar lista no NO PRINCIPAL*/
        BoxPair noTeste = new BoxPair<>("teste", listaFilhosTeste);

        System.out.println("teste");
        for (BoxPair no : (List<BoxPair>) noTeste.getValue()) {
            imprimir(no);
        }

    }
    
        private static void imprimir(JSONObject obJson) {
        Iterator<String> keys = obJson.keys();
        while (keys.hasNext()) {
            String keyName = keys.next();
            Object val = obJson.get(keyName);
        }
    }



    private static void imprimir(BoxPair no) {
        System.out.println("::::::::::::::::::::::::");
        System.out.println("chave::" + no.getKey());
        if (no.getValue().getClass() == ArrayList.class) {
            System.out.println("========================");
            for (BoxPair subno : (List<BoxPair>) no.getValue()) {
                imprimir(subno);
            }
        } else {
            System.out.println("valor::" + no.getValue());
        }

    }
}
